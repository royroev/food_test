import Reflux from 'reflux';
import {Actions} from '../actions';
let CartStore = Reflux.createStore({
    init: function() {
        this.listenTo(Actions.addToCard,this.onAddToCart);
        this.listenTo(Actions.removeFromCard,this.onRemoveFromCard);
        this.total = 0;
        this.buyItems = [];
    },
    onAddToCart(item) {
    this.total += item.price;
    let flag = true;
    var buyItems = this.buyItems;
    for (var i = 0; i < buyItems.length; i++) {
        if (buyItems[i].id == item.id) {
            flag = false;
            buyItems[i].col++;
            break;
        }
    }
    if (flag) {
        item.col = 1;
        buyItems.push(item);
    }
    this.trigger();

},
onRemoveFromCard(id) {
    var buyItems = this.buyItems, self = this;
    for (var i = 0; i < buyItems.length; i++) {
        var buyItem = buyItems[i];
        if (buyItem.id == id) {
            self.total -= buyItem.price;
            buyItem.col--;
            if (buyItem.col == 0)
                buyItems.splice(i, 1)
            break;
        }
    }
    this.trigger();
}
});
export default CartStore
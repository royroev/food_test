import Reflux from 'reflux';
var Actions = Reflux.createActions(["addToCard", "removeFromCard"]);
export {Actions}
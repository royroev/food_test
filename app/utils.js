export let styles = {
    item: {
        padding: '2px 6px',
        cursor: 'default'
    },

    isActive: {
        color: 'white',
        background: 'hsl(200, 50%, 50%)',
        padding: '2px 6px',
        cursor: 'default'
    },

    menu: {
        border: 'solid 1px #ccc'
    }
};


export function matchStateToTerm (state, value) {
    return (
        state.name.toLowerCase().indexOf(value.toLowerCase()) !== -1
    )
}

export function fakeRequest (value) {
    if (value === '')
        return getStates();
    var items = getStates().filter((state) => {
        return matchStateToTerm(state, value)
    });
    return items
}

export function getStates() {
    return [
        { id: 1, name: "Слова"},
        { id: 2, name: "Которые"},
        { id: 3, name: "Очень"},
        { id: 4, name: "Сложно"},
        { id: 5, name: "Придумывать"},
        { id: 6, name: "Находятся"},
        { id: 7, name: "В объекте"},
        { id: 8, name: "Автокомплит"},
        { id: 9, name: "СмОжет"},
        { id: 10, name: "Их"},
        { id: 11, name: "Найти"},
        { id: 12, name: "Если"},
        { id: 13, name: "Все"},
        { id: 14, name: "Правильно"},
        { id: 15, name: "Сделал"},
        { id: 16, name: "В Задании"}
    ]
}

import React from 'react';
import Reflux from 'reflux';
import CartStore from '../stores/CartStore';
import Modal from 'react-modal';
import {Actions} from '../actions';

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

let Cart = React.createClass({
    mixins : [Reflux.connect(CartStore, "buyItems")],
    getInitialState : function() {
        return {
            modalIsOpen: false
        }
    },
    componentDidMount : function() {
        this.listenTo(CartStore, this.chan);
    },
    openModal: function() {
        this.setState({modalIsOpen: true});
    },
    closeModal: function() {
        this.setState({modalIsOpen: false});
    },
    remove : function(id) {
        Actions.removeFromCard(id)
    },
    render : function() {
        return <div className="cart">
            <div>Товаров: {CartStore.buyItems.length} на сумму {CartStore.total} рублей.
                {(!!CartStore.buyItems.length) ?
                    <a className="go-to-cart" onClick={this.openModal}>Перейти в корзину</a> :
                    null
                }
            </div>
            <Modal isOpen={this.state.modalIsOpen} onRequestClose={this.closeModal} style={customStyles} >
                <h2>Корзина</h2>
                <button onClick={this.closeModal}>Закрыть</button>
                {(CartStore.buyItems.length == 0) ?
                    <p>Корзина пуста</p> :
                    <ul>
                        {CartStore.buyItems.map((it, index) => {
                            return <li key={index}>{it.name} ({it.price} руб.) - {it.col} шт. <a onClick={this.remove.bind(this, it.id)}>Удалить</a></li>
                        })}
                    </ul>
                }
                <p>Итого : {CartStore.total}</p>
                <button>Оплатить</button>
            </Modal>
        </div>
    },
    chan : function() {
        this.render()
    }
});
export {Cart}
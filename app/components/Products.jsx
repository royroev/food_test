import React from 'react';
import {Actions} from '../actions';

export default class Products extends React.Component {
    render () {
        return <div>
            {this.state.items.map((it, key) => {
                return <div className="item" key={key}>
                    <h1>{it.name} - {it.price} руб.</h1>
                    <a className="add-to-cart" data-price={it.price} onClick={this.addToCart.bind(this, it)}>
                        Добавить в корзину
                    </a>
                    </div>
            })}
            </div>
    }
    addToCart (item) {
        Actions.addToCard(item)
    }
    state = {
        items : [
            {id : 1, name : "one", price : 45},
            {id : 2, name : "two", price : 34},
            {id : 3, name : "three", price : 33},
            {id : 4, name : "four", price : 56}
        ]
    };
}
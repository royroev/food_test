import React from 'react';
import {Cart} from './Cart';
import Products from './Products';
import CartStore from '../stores/CartStore';

import t from '../../assets/main.less'

export default class App extends React.Component {
    handleChange (name, newValue) {
        this.setState((prevState) => {
            prevState[name] = newValue;
            return prevState
        });
    }
    linkState (name) {
        return {
            value: this.state[name],
            requestChange: this.handleChange.bind(this, name)
        }
    }
    render() {
        return <div>
            <h1>Пример</h1>
            <Cart />
            <Products />
        </div>;
    }
    state = {
        auto: ""
    };
}